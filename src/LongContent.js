import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';

class LongContent extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    let root = 'https://jsonplaceholder.typicode.com';

    $.ajax({
      url: root + '/comments',
      method: 'GET',
    }).then(function (data) {
      console.log(data);
      this.setState({
        data,
      });
    }.bind(this));
  }


  render() {
    let data = this.state.data;
    return (
      <div>
        This is a long block
        {data && data.map( (comment, index) => {
            if(index === 100)  return <Link to="/about" style={{backgroundColor: 'yellow'}}>About</Link>;
            return <h3>{comment.body}</h3>;
        }
        )}
      </div>
    );
  }
}

export default LongContent;
