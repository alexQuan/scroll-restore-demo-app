This project is mainly used for demonstrating scrolling with react router v4.

To start the project:

```
npm install
npm start
```

To see the restoring,
1. click on any of the topics and scroll down to find the `About` link.
2. click on the `About` link, the route will change
3. click on back button of the browser

You'll see the browser now back on the topic with scroll restored.
